unit Unit1;

{$mode objfpc}{$H+}

interface

uses
  Classes, SysUtils, FileUtil, Forms, Controls, Graphics, Dialogs, Buttons, StdCtrls;

type

  { TForm1 }

  TForm1 = class(TForm)
    BitBtn1: TBitBtn;
    ImageList1: TImageList;
    Memo1: TMemo;
    procedure BitBtn1Click(Sender: TObject);
    procedure FormCreate(Sender: TObject);
  private
    FOcultar:Boolean;
  end;

var
  Form1: TForm1;

implementation

{$R *.lfm}

{ TForm1 }

procedure TForm1.FormCreate(Sender: TObject);
begin
  FOcultar:=True;
  ImageList1.GetBitmap(0,BitBtn1.Glyph);
end;

procedure TForm1.BitBtn1Click(Sender: TObject);
begin
  if FOcultar then
  begin
    ImageList1.GetBitmap(1,BitBtn1.Glyph);
    BitBtn1.Caption:='Mostrar';
    Memo1.Visible:=False;
  end
  else
  begin
    ImageList1.GetBitmap(0,BitBtn1.Glyph);
    BitBtn1.Caption:='Ocultar';
    Memo1.Visible:=True;
  end;
  FOcultar:=not(FOcultar);
end;

end.

