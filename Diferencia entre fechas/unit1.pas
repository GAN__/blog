unit Unit1;

{$mode objfpc}{$H+}

interface

uses
    Classes, SysUtils, FileUtil, Forms, Controls, Graphics, Dialogs, StdCtrls, dateutils;

type

		{ TForm1 }

    TForm1 = class(TForm)
				btnDiferencia: TButton;
				btnCantidad: TButton;
				Label1: TLabel;
				Label2: TLabel;
				Label3: TLabel;
				Label4: TLabel;
				procedure btnCantidadClick(Sender: TObject);
    procedure btnDiferenciaClick(Sender: TObject);
    private
        fechainicio, fechafin:TDateTime;
        { private declarations }
    public
        { public declarations }
    end;

var
    Form1: TForm1;

implementation

{$R *.lfm}

{ TForm1 }

procedure TForm1.btnDiferenciaClick(Sender: TObject);
var
  Diferencia:TDateTime;
begin
  fechainicio:=StrToDateTime('09-07-2017 19:00');
  fechafin:=StrToDateTime('08-07-2017 02:27');
  Label1.Caption:='Fecha inicio: '+DateToStr(fechainicio);
  Label2.Caption:='Fecha fin: '+DateToStr(fechafin);
  Diferencia:=fechafin-fechainicio;
  Label3.Caption:=TimeToStr(TimeOf(Diferencia));
end;

procedure TForm1.btnCantidadClick(Sender: TObject);
var
  canthoras:Int64;
begin
  fechainicio:=StrToDateTime('05-07-2017 19:00');
  fechafin:=StrToDateTime('09-07-2017 02:27');
  canthoras:=HoursBetween(fechafin,fechainicio);
  Label4.Caption:=IntToStr(canthoras);
end;

end.

