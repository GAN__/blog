unit Unit1;

{$mode objfpc}{$H+}

interface

uses
  Classes, SysUtils, FileUtil, Forms, Controls, Graphics, Dialogs, ExtCtrls,
  Buttons;

type

  { TForm1 }

  TForm1 = class(TForm)
    BitBtn1: TBitBtn;
    IdleTimer1: TIdleTimer;
    procedure BitBtn1Click(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure IdleTimer1Timer(Sender: TObject);
  private

  public

  end;

var
  Form1: TForm1;

implementation

{$R *.lfm}

{ TForm1 }

procedure TForm1.FormCreate(Sender: TObject);
begin
  IdleTimer1.Enabled:=False;
  IdleTimer1.Interval:=10000;
end;

procedure TForm1.BitBtn1Click(Sender: TObject);
begin
  IdleTimer1.Enabled:=True;
  Hide;
end;

procedure TForm1.IdleTimer1Timer(Sender: TObject);
begin
  IdleTimer1.Enabled:=False;
  Show;
end;

end.

