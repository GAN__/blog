unit Unit1;

{$mode objfpc}{$H+}

interface

uses
    Classes, SysUtils, db, FileUtil, Forms, Controls, Graphics, Dialogs,
		DbCtrls, ZConnection, ZDataset;

type

		{ TForm1 }

    TForm1 = class(TForm)
				DataSource1: TDataSource;
				DBLookupComboBox1: TDBLookupComboBox;
				ZConnection1: TZConnection;
				ZQuery1: TZQuery;
				procedure FormCreate(Sender: TObject);
    private
        { private declarations }
    public
        { public declarations }
    end;

var
    Form1: TForm1;

implementation

{$R *.lfm}

{ TForm1 }

procedure TForm1.FormCreate(Sender: TObject);
begin
  ZConnection1.Database:='datos.db';
  ZConnection1.Connect;
  ZQuery1.SQL.Text:='SELECT * FROM turnos;';
  ZQuery1.Open;
  DBLookupComboBox1.DataSource:=DataSource1;
  DBLookupComboBox1.ListSource:=DataSource1;
  DBLookupComboBox1.DataField:='turid';
  DBLookupComboBox1.KeyField:='turid';
  DBLookupComboBox1.ListField:='turdesc';
end;

end.

