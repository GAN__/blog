unit Unit1;

{$mode objfpc}{$H+}

interface

uses
    Classes, SysUtils, Forms, FileUtil, Controls, Graphics, Dialogs, StdCtrls, RegExpr, LazFileUtils;

type

		{ TForm1 }

    TForm1 = class(TForm)
				Button1: TButton;
				lblSerial: TLabel;
				ListBox1: TListBox;
				procedure Button1Click(Sender: TObject);
				procedure FormCreate(Sender: TObject);
    private
        { private declarations }
    public
        { public declarations }
    end;

var
    Form1: TForm1;

implementation

{$R *.lfm}

{ TForm1 }

procedure TForm1.Button1Click(Sender: TObject);
var
  devList, disklist:TStringList;
  diskid, symlnk, RegexpInput:String;
  i:integer;
begin
  RegexpInput:='';
  if ListBox1.SelCount=0 then exit;
  diskid:=ListBox1.Items.Strings[ListBox1.ItemIndex];
  devList := TStringList.Create;
  devList.Sorted:=True;
  devList.Duplicates:=dupIgnore;
  disklist:=TStringList.Create;
  disklist.Sorted:=True;
  disklist.Duplicates:=dupIgnore;
  devList:=FindAllFiles('/dev/disk/by-id');
  for i:=0 to devList.Count-1 do
  begin
    symlnk:=ReadAllLinks(devList[i],False);
    disklist.Add(symlnk);
    if (symlnk=diskid) then RegexpInput:=RegexpInput+devList[i]+LineEnding;
  end;
  RegexObj:=TRegExpr.Create;
  RegexObj.Expression:='ata.*_([^ ]*)\n';
  if RegexObj.Exec(RegexpInput) then
    lblSerial.Caption:=RegexObj.Match[1];
	devList.Free;
  disklist.Free;
end;

procedure TForm1.FormCreate(Sender: TObject);
var
  devList, disklist:TStringList;
  RegexObj: TRegExpr;
  diskid, symlnk, RegexpInput:String;
  i:integer;
  maximo:Integer;
begin
  lblSerial.Caption:='';
  devList := TStringList.Create;
  devList.Sorted:=True;
  devList.Duplicates:=dupIgnore;
  disklist:=TStringList.Create;
  disklist.Sorted:=True;
  disklist.Duplicates:=dupIgnore;
  devList:=FindAllFiles('/dev/disk/by-id');
  for i:=0 to devList.Count-1 do
  begin
    symlnk:=ReadAllLinks(devList[i],False);
    disklist.Add(symlnk);
    if (symlnk=diskid) then RegexpInput:=RegexpInput+devList[i]+LineEnding;
  end;
  disklist.sort;
  ListBox1.Items:=disklist;
  maximo:=Length(ListBox1.Items.Strings[0]);
  for i:=ListBox1.Items.Count-1 downto 0 do
  begin
    if Length(ListBox1.Items.Strings[i])>maximo then
      ListBox1.Items.Delete(i);
	end;
	devList.Free;
  disklist.Free;
end;


end.

