unit Unit1;

{$mode objfpc}{$H+}

interface

uses
  Classes, SysUtils, Forms, Controls, Graphics, Dialogs, StdCtrls;

type

  { TForm1 }

  TForm1 = class(TForm)
    Button1: TButton;
    Button2: TButton;
    Memo1: TMemo;
    TaskDialog1: TTaskDialog;
    procedure Button1Click(Sender: TObject);
    procedure Button2Click(Sender: TObject);
  private

  public

  end;

var
  Form1: TForm1;

implementation

{$R *.lfm}

{ TForm1 }

procedure TForm1.Button1Click(Sender: TObject);
var
  i:integer;
  SeguirPreguntando:Boolean=True;
begin
  for i:=1 to 200 do
  begin
    if (i mod 10) = 0 then
      if SeguirPreguntando then
        if TaskDialog1.Execute then
          if (TaskDialog1.ModalResult=mrYes) or ((tfVerificationFlagChecked in TaskDialog1.Flags) and (TaskDialog1.ModalResult=mrYes) )then
            SeguirPreguntando:=not (tfVerificationFlagChecked in TaskDialog1.Flags)
          else
            Break;
    Memo1.Lines.Add(i.ToString);
    Application.ProcessMessages;
    Sleep(50);
  end;
end;

procedure TForm1.Button2Click(Sender: TObject);
begin
  Close;
end;

end.

