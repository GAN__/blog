unit Unit1;

{$mode objfpc}{$H+}

interface

uses
  Classes, SysUtils, FileUtil, Forms, Controls, Graphics, Dialogs, ShellCtrls,
  StdCtrls, Buttons;

type

  { TForm1 }

  TForm1 = class(TForm)
    BitBtn1: TBitBtn;
    Memo1: TMemo;
    ShellTreeView1: TShellTreeView;
    procedure BitBtn1Click(Sender: TObject);
  private

  public

  end;

var
  Form1: TForm1;

implementation

{$R *.lfm}

{ TForm1 }

procedure TForm1.BitBtn1Click(Sender: TObject);
var
  sl:TStringList;
begin
  Memo1.Clear;
  sl:=TStringList.Create;
  sl.OwnsObjects:=True; //evita memory leaks
  ShellTreeView1.GetFilesInDir(ShellTreeView1.Path,'*.*',[otNonFolders],sl,fstAlphabet);
  Memo1.Text:=sl.Text;
  FreeAndNil(sl);
end;

end.

