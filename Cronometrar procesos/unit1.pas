unit Unit1;

{$mode objfpc}{$H+}

interface

uses
    Classes, SysUtils, FileUtil, DateTimePicker, Forms, Controls, Graphics,
		Dialogs, StdCtrls, dateutils;

type

		{ TForm1 }

    TForm1 = class(TForm)
				Button1: TButton;
				Button2: TButton;
				CheckBox1: TCheckBox;
				dtpEmpieza: TDateTimePicker;
				dtpFinaliza: TDateTimePicker;
				dtpTranscurrido: TDateTimePicker;
				edImpps: TEdit;
				Label1: TLabel;
				Label2: TLabel;
				Label3: TLabel;
				Label4: TLabel;
				Memo1: TMemo;
				procedure Button1Click(Sender: TObject);
				procedure Button2Click(Sender: TObject);
    private
        procedure Empezar;
        procedure Finalizar;
        { private declarations }
    public
        { public declarations }
    end;

var
    Form1: TForm1;

implementation

{$R *.lfm}

{ TForm1 }

procedure TForm1.Button1Click(Sender: TObject);
var
  i:Integer;
begin
  Empezar;
  if CheckBox1.Checked then
  begin
    for i:=1 to 10000 do
      begin
        Application.ProcessMessages;
        Memo1.Lines.Add(IntToStr(i));
  		end;
	end
  else
  begin
	for i:=1 to 10000 do
      Memo1.Lines.Add(IntToStr(i));
  end;
  Finalizar;
end;

procedure TForm1.Button2Click(Sender: TObject);
begin
  Memo1.Clear;
end;

procedure TForm1.Empezar;
begin
  dtpEmpieza.Time:=Now;
end;

procedure TForm1.Finalizar;

begin
  dtpFinaliza.Time:=Now;
  dtpTranscurrido.Time:=dtpFinaliza.Time-dtpEmpieza.Time;
  edImpps.Text:=FloatToStr((10000/((SecondOf(dtpTranscurrido.Time)+((MilliSecondOf(dtpTranscurrido.Time)/1000))))));
end;

end.

