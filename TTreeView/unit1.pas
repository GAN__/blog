unit Unit1;

{$mode objfpc}{$H+}

interface

uses
  Classes, SysUtils, FileUtil, Forms, Controls, Graphics, Dialogs, Buttons,
  ComCtrls;

type

  { TForm1 }

  TForm1 = class(TForm)
    BCargarIconos: TBitBtn;
    BExpandir: TBitBtn;
    BContraer: TBitBtn;
    BSeleccionados: TBitBtn;
    BNiveles: TBitBtn;
    BNuevo: TBitBtn;
    BModificar: TBitBtn;
    BEliminar: TBitBtn;
    BBorrarArbol: TBitBtn;
    BOrdenar: TBitBtn;
    BitBtn6: TBitBtn;
    BitBtn7: TBitBtn;
    ImageList1: TImageList;
    TrackBar1: TTrackBar;
    TreeView1: TTreeView;
    procedure BBorrarArbolClick(Sender: TObject);
    procedure BCargarIconosClick(Sender: TObject);
    procedure BContraerClick(Sender: TObject);
    procedure BEliminarClick(Sender: TObject);
    procedure BExpandirClick(Sender: TObject);
    procedure BitBtn6Click(Sender: TObject);
    procedure BitBtn7Click(Sender: TObject);
    procedure BNivelesClick(Sender: TObject);
    procedure BOrdenarClick(Sender: TObject);
    procedure BModificarClick(Sender: TObject);
    procedure BNuevoClick(Sender: TObject);
    procedure BSeleccionadosClick(Sender: TObject);
    procedure TrackBar1Change(Sender: TObject);
    procedure TreeView1Compare(Sender: TObject; Node1, Node2: TTreeNode; var Compare: Integer);
    procedure TreeView1CustomDrawItem(Sender: TCustomTreeView; Node: TTreeNode;
      State: TCustomDrawState; var DefaultDraw: Boolean);
  private

  public

  end;

var
  Form1: TForm1;

implementation

{$R *.lfm}

{ TForm1 }

procedure TForm1.BNuevoClick(Sender: TObject);
var
  nombre:String;
begin
  nombre:=InputBox('Crear un nodo','Nombre: ','');
  if nombre<>'' then
    begin
      if TreeView1.Selected<>nil then
        begin
          TreeView1.Items.AddChild(TreeView1.Selected, nombre);
          TreeView1.Selected.Expanded:=True;
        end
      else
        TreeView1.Items.Add(nil, nombre);
    end;
  TreeView1.Selected:=nil;
end;

procedure TForm1.BSeleccionadosClick(Sender: TObject);
var
  i:Integer;
  s:TStringList;
begin
  s:=TStringList.Create;
  for i:=0 to TreeView1.Items.Count-1 do
    if TreeView1.Items[i].Selected then s.Add(TreeView1.Items[i].Text);
  ShowMessage(s.Text);
  FreeAndNil(s);
end;

procedure TForm1.TrackBar1Change(Sender: TObject);
begin
  TreeView1.Indent:=TrackBar1.Position;
end;

procedure TForm1.TreeView1Compare(Sender: TObject; Node1, Node2: TTreeNode; var Compare: Integer);
begin
  Compare:=CompareText(Node1.Text,Node2.Text);
end;

procedure TForm1.TreeView1CustomDrawItem(Sender: TCustomTreeView; Node: TTreeNode; State: TCustomDrawState; var DefaultDraw: Boolean);
begin
  if Node.Level=1 then
    begin
      Sender.Canvas.Font.Color:=clBlue;  //tvoThemeDraw:=False
      Sender.Canvas.Font.Style:=[fsBold];
    end
  else
    begin
      Sender.Canvas.Font.Color:=clBlack;
      Sender.Canvas.Font.Style:=[];
    end;
  if cdsFocused in State then Sender.Canvas.Font.Color:=clWhite;
end;

procedure TForm1.BModificarClick(Sender: TObject);
var
  nombre:String;
begin
  if TreeView1.Selected<>nil then
  begin
    nombre:=InputBox('Modificar un nodo','Nombre: ',TreeView1.Selected.Text);
    if nombre<>'' then TreeView1.Selected.Text:=nombre;
  end;
  TreeView1.Selected:=nil;
end;

procedure TForm1.BEliminarClick(Sender: TObject);
begin
  if TreeView1.Selected<>nil then TreeView1.Selected.Delete;
end;

procedure TForm1.BExpandirClick(Sender: TObject);
begin
  TreeView1.FullExpand;
end;

procedure TForm1.BitBtn6Click(Sender: TObject);
begin
  TreeView1.LoadFromFile('arbol1.txt');
end;

procedure TForm1.BitBtn7Click(Sender: TObject);
begin
  TreeView1.SaveToFile('arbol1.txt');
end;

procedure TForm1.BNivelesClick(Sender: TObject);
var
  i:Integer;
begin
  for i:=0 to TreeView1.Items.Count-1 do
    TreeView1.Items[i].Text:=TreeView1.Items[i].Text+' Niv.'+IntToStr(TreeView1.Items[i].Level);
end;

procedure TForm1.BOrdenarClick(Sender: TObject);
begin
  TreeView1.AlphaSort;
end;

procedure TForm1.BBorrarArbolClick(Sender: TObject);
begin
  TreeView1.Items.Clear;
end;

procedure TForm1.BCargarIconosClick(Sender: TObject);
var
  i:Integer;
begin
  for i:=0 to TreeView1.Items.Count-1 do
    if TreeView1.Items[i].Level=0 then
      begin
        TreeView1.Items[i].ImageIndex:=0;
        TreeView1.Items[i].SelectedIndex:=0;
      end
    else
      begin
        TreeView1.Items[i].ImageIndex:=1;
        TreeView1.Items[i].SelectedIndex:=1;
      end;
end;

procedure TForm1.BContraerClick(Sender: TObject);
begin
  TreeView1.FullCollapse;
end;

end.

