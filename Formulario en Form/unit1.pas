unit Unit1;

{$mode objfpc}{$H+}

interface

uses
  Classes, SysUtils, FileUtil, Forms, Controls, Graphics, Dialogs, StdCtrls;

type

  { TForm1 }

  TForm1 = class(TForm)
    Button1: TButton;
    Button2: TButton;
    Button3: TButton;
    procedure Button1Click(Sender: TObject);
    procedure Button2Click(Sender: TObject);
    procedure Button3Click(Sender: TObject);
    procedure FormClose(Sender: TObject; var CloseAction: TCloseAction);
    procedure FormCreate(Sender: TObject);
  private
    sl1:TStringList;
    sl2:TStringList;
    sl3:TStringList;
    procedure VerLista(sl:TStringList);
  public

  end;

var
  Form1: TForm1;

implementation

{$R *.lfm}

{ TForm1 }

procedure TForm1.FormCreate(Sender: TObject);
var
  i:Integer;
begin
  sl1:=TStringList.Create;
  sl2:=TStringList.Create;
  sl3:=TStringList.Create;
  for i:=0 to 99 do
  begin
    sl1.Add(IntToStr(i));
    sl2.Add(IntToStr(i*10));
    sl3.Add(IntToStr(i*100));
  end;
end;

procedure TForm1.VerLista(sl: TStringList);
var
  F:TForm;
  mm:TMemo;
begin
  F:=TForm.Create(nil);
  F.Height:=380;
  F.Width:=500;
  F.Position:=poMainFormCenter;
  mm:=TMemo.Create(nil);
  mm.Text:=sl.Text;
  mm.Parent:=F;
  mm.Align:=alClient;
  mm.ScrollBars:=ssAutoBoth;
  F.ShowModal;
  FreeAndNil(mm);
  FreeAndNil(F);
end;

procedure TForm1.FormClose(Sender: TObject; var CloseAction: TCloseAction);
begin
  CloseAction:=caFree;
end;

procedure TForm1.Button1Click(Sender: TObject);
begin
  VerLista(sl1);
end;

procedure TForm1.Button2Click(Sender: TObject);
begin
  VerLista(sl2);
end;

procedure TForm1.Button3Click(Sender: TObject);
begin
  VerLista(sl3);
end;

end.

