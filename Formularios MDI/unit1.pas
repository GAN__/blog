unit Unit1;

{$mode objfpc}{$H+}

interface

uses
    Classes, SysUtils, FileUtil, Forms, Controls, Graphics, Dialogs, StdCtrls, Unit2,
		Menus;

type

		{ TForm1 }

    TForm1 = class(TForm)
				Label1: TLabel;
				MainMenu1: TMainMenu;
				MenuItem1: TMenuItem;
				MenuItem2: TMenuItem;
				MenuItem3: TMenuItem;
				MenuItem4: TMenuItem;
				MenuItem5: TMenuItem;
				MenuItem6: TMenuItem;

		procedure FormCreate(Sender: TObject);
    procedure MenuItem4Click(Sender: TObject);
		procedure MenuItem5Click(Sender: TObject);
    private
        { private declarations }
    public
        { public declarations }
    end;

var
    Form1: TForm1;

implementation

{$R *.lfm}

{ TForm1 }

procedure TForm1.MenuItem4Click(Sender: TObject);
begin
  Form2.Show;
end;

procedure TForm1.FormCreate(Sender: TObject);
begin
  Show;
  Form2.Parent:=Form1;
end;

procedure TForm1.MenuItem5Click(Sender: TObject);
begin
  ShowMessage('Hola.');
end;


end.

