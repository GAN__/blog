unit principal;

{$mode objfpc}{$H+}

interface

uses
  Classes, SysUtils, FileUtil, Forms, Controls, LCLIntf, Graphics, Dialogs, ShellCtrls, Buttons,
  StdCtrls, ComCtrls, ExtCtrls, Interfaces;

type

  { TForm1 }

  TForm1 = class(TForm)
    BSalir: TBitBtn;
    cbOcultos: TCheckBox;
    cbDirectorios: TCheckBox;
    Label1: TLabel;
    Label2: TLabel;
    lTotArch: TLabel;
    Label4: TLabel;
    lTotSubDir: TLabel;
    lTam: TLabel;
    arbol: TShellTreeView;
    Memo1: TMemo;
    Panel1: TPanel;
    Panel2: TPanel;
    Splitter1: TSplitter;
    procedure arbolClick(Sender: TObject);
    procedure BSalirClick(Sender: TObject);
    procedure cbDirectoriosChange(Sender: TObject);
    procedure cbOcultosChange(Sender: TObject);
    procedure FormClose(Sender: TObject; var CloseAction: TCloseAction);
    procedure FormCreate(Sender: TObject);
  private
    procedure SetPath;
    procedure MostrarArchivos;
    {//0 Dir y Oc - 1 Dir - 2 No Dir y No Oc - 3 No Dir y Oc}
    function GetOpciones:Byte;
    function Tamanio(var sDenominacion: String; nByte: Int64):Single;
  public

  end;


var
  Form1: TForm1;
  APath:String;
  ARoot:String;
  ind:Integer;
  opciones:Byte;
  TotArchivos:Integer=0;
  TotCarpetas:Integer=0;
  TotTamanio:Int64=0;

implementation

{$R *.lfm}

{ TForm1 }

procedure TForm1.FormCreate(Sender: TObject);
begin
  SetPath;
  arbol.Path:=APath;   //le agrega un pathdelimiter al final de aPath
  MostrarArchivos;
  Label1.Caption:=arbol.Path;
end;

procedure TForm1.MostrarArchivos;
var
  Arch:TSearchRec;
  den:String='';
  linea:String='';
begin
  TotArchivos:=0;
  TotCarpetas:=0;
  TotTamanio:=0;
  Memo1.Clear;
  opciones:=GetOpciones;
  case opciones of
    0 : if FindFirst(arbol.Path+'*',faHidden or faDirectory,Arch)<>0 then Exit;  //Si encontró devuelve 0 cero.
    1 : if FindFirst(arbol.Path+'*',faDirectory,Arch)<>0 then Exit;
    2 : if FindFirst(arbol.Path+'*',not faHidden and not faDirectory,Arch)<>0 then Exit;
    3 : if FindFirst(arbol.Path+'*',faHidden and not faDirectory,Arch)<>0 then Exit;
  end;
  repeat
    if ((Arch.Name<>'.') and (Arch.Name<>'..')) then
    begin
      linea:=Format('%0:-40s',[' '+Arch.Name]);
      if (Arch.Attr and  faDirectory)<>0 then
        begin
          linea:=linea+Format('%0:12s',['[Dir]']);
          Inc(TotCarpetas);
        end
      else
        begin
          linea:=linea+Format('%8.2n',[Tamanio(den,Arch.Size)])+Format('%0:4s',[den]);
          Inc(TotArchivos);
          TotTamanio:=TotTamanio+Arch.Size;
        end;
      linea:=linea+Format('%0:20s',[DateTimeToStr(FileDateToDateTime(Arch.Time))]);
      Memo1.Lines.Add(linea);
    end;
    lTotArch.Caption:=IntToStr(TotArchivos);
    lTam.Caption:=FormatFloat('0.00',(Tamanio(den,TotTamanio)))+' '+den;
    lTotSubDir.Caption:=IntToStr(TotCarpetas);
  until FindNext(Arch)<>0;
  FindClose(Arch);
end;

function TForm1.GetOpciones: Byte;
begin
  if (cbDirectorios.Checked and cbOcultos.Checked) then Result:=0;
  if (cbDirectorios.Checked and not(cbOcultos.Checked)) then Result:=1;
  if ((not(cbDirectorios.Checked)) and (not(cbOcultos.Checked))) then Result:=2;
  if ((not(cbDirectorios.Checked)) and cbOcultos.Checked) then Result:=3;
end;

function TForm1.Tamanio(var sDenominacion: String; nByte: Int64): Single;
CONST
  KBytes=1024;
  MBytes=1024*1024;
  GBytes=1024*1024*1024;
  TBytes=1024*1024*1024*1024;
  PBytes=1024*1024*1024*1024*1024;
begin
  case nByte of
    0..KBytes-1 : begin
                    sDenominacion:='B';
                    Result:=nByte;
                  end;
    KBytes..MBytes-1 : begin
                         sDenominacion:='KB';
                         Result:=nByte/KBytes;
                       end;
    MBytes..GBytes-1 : begin
                         sDenominacion:='MB';
                         Result:=nByte/MBytes;
                       end;
    GBytes..TBytes-1 : begin
                         sDenominacion:='GB';
                         Result:=nByte/GBytes;
                       end;
    TBytes..PBytes-1 : begin
                         sDenominacion:='TB';
                         Result:=nByte/TBytes;
                       end;
  else
    begin
      sDenominacion:='??';
      Result:=1;
    end;
  end;
end;


procedure TForm1.SetPath;
begin
  {$IFDEF WINDOWS}
  APath:=ExtractFilePath(Application.Location);
  {$ELSE}
  APath:=GetEnvironmentVariable('HOME');
  ARoot:=APath;
  //arbol.Root:=ARoot; Si queremos que la raíz del árbol sea la home del usuario.
  {$ENDIF}
end;

procedure TForm1.BSalirClick(Sender: TObject);
begin
  Close;
end;

procedure TForm1.cbDirectoriosChange(Sender: TObject);
begin
  MostrarArchivos;
end;

procedure TForm1.cbOcultosChange(Sender: TObject);
begin
  MostrarArchivos;
end;

procedure TForm1.FormClose(Sender: TObject; var CloseAction: TCloseAction);
begin
  CloseAction:=caFree;
end;

procedure TForm1.arbolClick(Sender: TObject);
begin
  if not Assigned(arbol.Selected) then Exit;
  Label1.Caption:=arbol.Path;
  MostrarArchivos;
end;

end.

{    File attributes
Const
  faReadOnly   = $00000001;
  faHidden     = $00000002 platform;
  faSysFile    = $00000004 platform;
  faVolumeId   = $00000008 platform deprecated;
  faDirectory  = $00000010;
  faArchive    = $00000020;
  faNormal     = $00000080;
  faTemporary  = $00000100 platform;
  faSymLink    = $00000400 platform;
  faCompressed = $00000800 platform;
  faEncrypted  = $00004000 platform;
  faVirtual    = $00010000 platform;
  faAnyFile    = $000001FF;
}
