unit Unit1;

{$mode objfpc}{$H+}

interface

uses
    Classes, SysUtils, Forms, Controls, Graphics, Dialogs, StdCtrls, Buttons;

type

		{ TForm1 }

    TForm1 = class(TForm)
				btnQuitarL1: TBitBtn;
				btnQuitarL2: TBitBtn;
				btnTodoL1: TBitBtn;
				btnAgregar: TBitBtn;
				btnAgregarTodos: TBitBtn;
				btnQuitar: TBitBtn;
				btnQuitarTodos: TBitBtn;
				btnTodoL2: TBitBtn;
				ListBox1: TListBox;
				ListBox2: TListBox;
				StaticText1: TStaticText;
				StaticText2: TStaticText;
				procedure btnAgregarClick(Sender: TObject);
				procedure btnAgregarTodosClick(Sender: TObject);
				procedure btnQuitarClick(Sender: TObject);
				procedure btnQuitarL1Click(Sender: TObject);
				procedure btnQuitarL2Click(Sender: TObject);
				procedure btnQuitarTodosClick(Sender: TObject);
				procedure btnTodoL1Click(Sender: TObject);
				procedure btnTodoL2Click(Sender: TObject);
				procedure ListBox2DragDrop(Sender, Source: TObject; X, Y: Integer);
				procedure ListBox2DragOver(Sender, Source: TObject; X, Y: Integer;
						State: TDragState; var Accept: Boolean);
    private
        { private declarations }
    public
        function YaExiste (elemento:String):Boolean;
        { public declarations }
    end;

var
    Form1: TForm1;

implementation

{$R *.lfm}

{ TForm1 }

procedure TForm1.btnAgregarClick(Sender: TObject);
var
  i:Integer;
begin
  if ListBox1.Count<1 then exit; //Si no hay elementos no hace nada
  for i:=0 to ListBox1.Count-1 do  //Recorre todo el ListBox1
    if ListBox1.Selected[i] then  //Si el ítem está seleccionado
       if not (YaExiste(ListBox1.Items.Strings[i])) then //Si el ítem a agregar ya existe no lo agrega
         ListBox2.AddItem(ListBox1.Items.Strings[i],ListBox2);  //Agrega el ítem
  ListBox1.ClearSelection;
end;

procedure TForm1.btnAgregarTodosClick(Sender: TObject);
begin
  ListBox2.Items:=ListBox1.Items;
  ListBox1.ClearSelection;
end;

procedure TForm1.btnQuitarClick(Sender: TObject);
var
  i:Integer;
begin
  if ListBox2.SelCount > 0 then  //Si hay ítems seleccionados
    for i:=ListBox2.Items.Count-1 downto 0 do  //Recorre todo el ListBox2
      if ListBox2.Selected[i] then   //Si el ítem está seleccionado
        ListBox2.Items.Delete(i);  //Borra el ítem
end;

procedure TForm1.btnQuitarL1Click(Sender: TObject);
begin
  ListBox1.ClearSelection;
end;

procedure TForm1.btnQuitarL2Click(Sender: TObject);
begin
  ListBox2.ClearSelection;
end;

procedure TForm1.btnQuitarTodosClick(Sender: TObject);
begin
  ListBox2.Clear;
end;

procedure TForm1.btnTodoL1Click(Sender: TObject);
begin
  ListBox1.SelectAll;
end;

procedure TForm1.btnTodoL2Click(Sender: TObject);
begin
  ListBox2.SelectAll;
end;

procedure TForm1.ListBox2DragDrop(Sender, Source: TObject; X, Y: Integer);
begin
  btnAgregarClick(Self);
end;

procedure TForm1.ListBox2DragOver(Sender, Source: TObject; X, Y: Integer;
		State: TDragState; var Accept: Boolean);
begin
  Accept:=(Source is TListBox);
end;

function TForm1.YaExiste(elemento: String): Boolean;
var
  i:Integer;
  ret:Boolean;
begin
  ret:=False;
  for i:=0 to ListBox2.Count-1 do
    if elemento=ListBox2.Items.Strings[i] then ret:=True;
  YaExiste:=ret;
end;

end.

