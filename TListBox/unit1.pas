unit Unit1;

{$mode objfpc}{$H+}

interface

uses
    Classes, SysUtils, FileUtil, Forms, Controls, Graphics, Dialogs, StdCtrls,
		Buttons;

type

		{ TForm1 }

    TForm1 = class(TForm)
				btnRecorrer: TBitBtn;
				btnListAMemo: TBitBtn;
				btnAgregar: TBitBtn;
				btnEliminar: TBitBtn;
				btnOrdenar: TBitBtn;
				btnBorrarTodo: TBitBtn;
				cbDuplicados: TCheckBox;
				edAgregar: TEdit;
				edSeleccionado: TEdit;
				lblSeleccionado: TLabel;
				ListBox1: TListBox;
				Memo1: TMemo;
				procedure btnRecorrerClick(Sender: TObject);
    procedure btnAgregarClick(Sender: TObject);
				procedure btnBorrarTodoClick(Sender: TObject);
				procedure btnEliminarClick(Sender: TObject);
				procedure btnListAMemoClick(Sender: TObject);
				procedure btnOrdenarClick(Sender: TObject);
				procedure FormCreate(Sender: TObject);
				procedure ListBox1Click(Sender: TObject);
    private
        function YaExiste (elemento: String): Boolean;
        { private declarations }
    public
        { public declarations }
    end;

var
    Form1: TForm1;

implementation

{$R *.lfm}

{ TForm1 }

procedure TForm1.btnAgregarClick(Sender: TObject);
begin
  if Length(Trim(edAgregar.Text))<1 then exit;
  //Trim elimina espacios en blanco, Length el tamaño.
  //Si ingresa un dato en blanco no hace nada.
  if not (cbDuplicados.Checked) then if YaExiste(edAgregar.Text) then exit;
  //Si no se marcó Permitir duplicados entonces se llama a la función YaExiste, si devuelve True no se agrega.
  ListBox1.AddItem(edAgregar.Text,ListBox1);
  //Además del string a agregar hay que especificar el objeto.
  edAgregar.Clear;
  //Limpia el TEdit. edAgregar.Text:='' también es válido.
end;

procedure TForm1.btnRecorrerClick(Sender: TObject);
var
  i:Integer;
begin
  Memo1.Clear;
  Memo1.Lines.Add('');
  Memo1.Lines.Add('for i:=0 to ListBox1.Count-1 do'+#13#10+'  Memo1.Lines.Add(ListBox1.Items.Strings[i]);'+#13#10);
  for i:=0 to ListBox1.Count-1 do
    //TListBox es base 0 (cero) por eso desde 0 hasta cantidad-1
    Memo1.Lines.Add('ListBox1.Items.Strings['+IntToStr(i)+']: '+ListBox1.Items.Strings[i]);
    //ListBox1.Items.String[i] así se accede al texto de cada ítem.
end;

procedure TForm1.btnBorrarTodoClick(Sender: TObject);
begin
  ListBox1.Clear;
  //Limpia la ListBox
end;

procedure TForm1.btnEliminarClick(Sender: TObject);
begin
  if ListBox1.ItemIndex>=0 then ListBox1.Items.Delete(ListBox1.ItemIndex);
  //Si hay algún ítem seleccionado, entonces ItemIndex tendrá un valor mayor o igual a cero, caso contrario será -1.
end;

procedure TForm1.btnListAMemoClick(Sender: TObject);
begin
  Memo1.Lines.Add('Memo1.Lines:=ListBox1.Items;');
  Memo1.Lines:=ListBox1.Items;
  //Ya la propiedades Lines e Items son del mismo tipo TStrings basta con asignar una a la otra.
end;

procedure TForm1.btnOrdenarClick(Sender: TObject);
begin
  ListBox1.Sorted:=True;
  ListBox1.Sorted:=False;
  //El False se agrega para que si se agrega un nuevo elemento lo agregue al final y se pueda volver a ordenar.
end;

procedure TForm1.FormCreate(Sender: TObject);
begin
  ListBox1.AddItem('Devuan',ListBox1);
  ListBox1.AddItem('Linux mint',ListBox1);
  ListBox1.AddItem('Gentoo',ListBox1);
  ListBox1.AddItem('Arch Linux',ListBox1);
  ListBox1.AddItem('Ubuntu',ListBox1);
  ListBox1.AddItem('Debian',ListBox1);
  ListBox1.AddItem('Mangaro',ListBox1);
end;

procedure TForm1.ListBox1Click(Sender: TObject);
begin
  edSeleccionado.Text:=ListBox1.Items.Strings[ListBox1.ItemIndex];
  //Se accede al texto Items.Strings y el índice lo tomamos de la propia lista ItemIndex que es el ítem seleccionado.
end;

function TForm1.YaExiste(elemento: String): Boolean;
//elemento es el texto de edAgregar
var
  i:Integer;
  ret:Boolean;
begin
  ret:=False;
  for i:=0 to listbox1.Count-1 do
    if elemento=listbox1.Items.Strings[i] then ret:=True;
  //Recorremos toda la lista buscando si ya existe.
  YaExiste:=ret;
end;

end.

