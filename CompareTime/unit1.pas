unit Unit1;

{$mode objfpc}{$H+}

interface

uses
    Classes, SysUtils, FileUtil, DateTimePicker, Forms, Controls, Graphics, dateutils,
		Dialogs, StdCtrls;

type

		{ TForm1 }

    TForm1 = class(TForm)
				btnComparar: TButton;
				Button1: TButton;
				Button2: TButton;
				Button3: TButton;
				dtpIng: TDateTimePicker;
				dtpEgr: TDateTimePicker;
				Label1: TLabel;
				Label2: TLabel;
				Memo1: TMemo;
				procedure btnCompararClick(Sender: TObject);
				procedure Button1Click(Sender: TObject);
				procedure Button2Click(Sender: TObject);
				procedure Button3Click(Sender: TObject);
    private
        { private declarations }
    public
        { public declarations }
    end;

var
    Form1: TForm1;

implementation

{$R *.lfm}

{ TForm1 }

procedure TForm1.btnCompararClick(Sender: TObject);
begin
   CASE CompareTime(dtpIng.Time,dtpEgr.Time) of
     1 : Memo1.Append('Llegó temprano');
     0 : Memo1.Append('Llego en horario.');
    -1 : Memo1.Append('Llegó tarde.');
	 end;
end;

procedure TForm1.Button1Click(Sender: TObject);
var
  dif:TTime;
begin
  dif:=dtpIng.Time-dtpEgr.Time;
  Memo1.Append(TimeToStr(dif));
end;

procedure TForm1.Button2Click(Sender: TObject);
var
  dif:Int64;
  tiempo:TTime;
begin
  dif:=MinutesBetween(dtpIng.DateTime,dtpEgr.DateTime);
  Memo1.Append(IntToStr(dif));
  tiempo:=dtpIng.Time-dtpEgr.Time;
  Memo1.Append(TimeToStr(tiempo));
end;

procedure TForm1.Button3Click(Sender: TObject);
var
  difh:Word;
  intIng,intEgr,intDif:Integer;
begin
  difh:=HourOf(dtpIng.Time)-HourOf(dtpEgr.Time);
  Memo1.Append(IntToStr(difh));
  intIng:=HourOf(dtpIng.Time);
  intEgr:=HourOf(dtpEgr.Time);
  intDif:=intIng-intEgr;
  Memo1.Append(IntToStr(intdif));
end;

end.

